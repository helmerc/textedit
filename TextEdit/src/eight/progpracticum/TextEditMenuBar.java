/**
 * 
 */
package eight.progpracticum;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Class that creates the menu bar for the text edit program.
 * @author Christopher Helmer
 * @version 1
 *
 */
@SuppressWarnings("serial")
public class TextEditMenuBar extends JMenuBar {
    /**
     * The menu selections for file.
     */
    private JMenu myFileMenu;
    /**
     * The menu selections for format.
     */
    private JMenu myFormatMenu;
    /**
     * The menu selections for help.
     */
    private JMenu myHelpMenu;
    /**
     * Action listener for opening or saving.
     */
    private FileAction myFileAction;
    /**
     * Action listener for font choices.
     */
    private TextAction myTextAction;
    /**
     * File chooser used to open or save files.
     */
    private JFileChooser myFilePicker;
    /**
     * Menu item to generate a new file.
     */
    private JMenuItem myNewFile;
    /**
     * Menu item to save the file.
     */
    private JMenuItem mySaveFile;
    /**
     * menu item to open a file.
     */
    private JMenuItem myOpenFile;
    /**
     * Menu item to print the file.
     */
    private JMenuItem myPrintFile;
    /**
     * Menu item to exit the file.
     */
    private JMenuItem myExitFile;
    /**
     * Menu item to change the font style.
     */
    private JMenuItem myStyle;
    /**
     * Menu item to change the font type. 
     */
    private JMenuItem myType;
    /**
     * Menu item to change the font size.
     */
    private JMenuItem myFontSize;
    /**
     * Menu item that shows a popup with program info.
     */
    private JMenuItem myAbout;
    /**
     * Check box menu item to choose word wrap.
     */
    private JCheckBoxMenuItem myWordWrap;
    /**
     * Current TextEditPanel.
     */
    private TextEditPanel myPanel;
    /**
     * Current JTextArea.
     */
    private JTextArea myArea;
    
    /**
     * Constructor for the TextEditMenuBar.
     * @param aPanel is the current TextEditPanel.
     * @custom.post A new instance of the TextEditMenuBar is created.
     */
    public TextEditMenuBar(final TextEditPanel aPanel) {
        super();
        myFileAction = new FileAction();
        myTextAction = new TextAction();
        myPanel = aPanel;
        myArea = myPanel.getMyTextArea();
        fileSetup();
        formatSetup();
        helpSetup();
    }
    /**
     * Helper method for the constructor that creates the file menu.
     */
    private void fileSetup() {
        myFileMenu = new JMenu("File");
        
        myNewFile = new JMenuItem("New");
        myOpenFile = new JMenuItem("Open");
        mySaveFile = new JMenuItem("Save");
        myPrintFile = new JMenuItem("Print");
        myExitFile = new JMenuItem("Exit");
        myNewFile.setAccelerator(KeyStroke.getKeyStroke("ctrl N"));
        myOpenFile.setAccelerator(KeyStroke.getKeyStroke("ctrl O"));
        mySaveFile.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        myPrintFile.setAccelerator(KeyStroke.getKeyStroke("ctrl P"));
        myExitFile.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));
        myNewFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent anEvent) {
                myArea.setText("");
            }
        });
        myPrintFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent anEvent) {
                try {
                    myArea.print(null, null);
                } catch (final PrinterException e) {
                    e.printStackTrace();
                }
            }
        });
        myOpenFile.addActionListener(myFileAction);
        mySaveFile.addActionListener(myFileAction);
        myExitFile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent anEvent) {
                System.exit(0);
            }
        });
        myFileMenu.add(myNewFile);
        myFileMenu.add(myOpenFile);
        myFileMenu.add(mySaveFile);
        myFileMenu.add(myPrintFile);
        myFileMenu.addSeparator();
        myFileMenu.add(myExitFile);
        add(myFileMenu);
    }
    
    /**
     * Helper method for the constructor that creates the format menu.
     */
    private void formatSetup() {
        myFormatMenu = new JMenu("Format");
        myWordWrap = new JCheckBoxMenuItem("Word wrap");
        myWordWrap.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent anEvent) {
                if (myWordWrap.isSelected()) {
                    myArea.setLineWrap(true);
                } else {
                    myArea.setLineWrap(false);
                }
            }
        });
        myStyle = new JMenuItem("Style");
        myType = new JMenuItem("Type");
        myFontSize = new JMenuItem("Size");
        myStyle.addActionListener(myTextAction);
        myType.addActionListener(myTextAction);
        myFontSize.addActionListener(myTextAction);
        myFormatMenu.add(myWordWrap);
        myFormatMenu.addSeparator();
        myFormatMenu.add(myStyle);
        myFormatMenu.add(myType);
        myFormatMenu.add(myFontSize);
        add(myFormatMenu);
    }
    
    /**
     * Helper method for the constructor that creates the help menu.
     */
    private void helpSetup() {
        myHelpMenu = new JMenu("Help");
        myAbout = myHelpMenu.add("About");
        myAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent anEvent) {
                JOptionPane.showMessageDialog(null, "Chris' Notepad", "Version 1.0", 
                                              JOptionPane.INFORMATION_MESSAGE);
            }
        });
        add(myHelpMenu);
    }
    
    /**
     * Method that puts the text from the file to be opened onto a new blank JTextArea.
     * @param aFile is the file to be read from.
     * @custom.post The file to be open has been opened to the text area.
     */
    public void openText(final File aFile) {
        final JTextArea area = myPanel.getMyTextArea();
        area.setText("");
        try {
            @SuppressWarnings("resource")
            final Scanner input = new Scanner(aFile);
            while (input.hasNext()) {
                area.append(input.nextLine() + "\n");
            }
        } catch (final IOException e) {
            System.out.println("file did not open");
        }
        
        
    }
    
    /**
     * Method used to save the current text to a file.
     * @param aType is the type of file being saved.
     * @param aPath is the path of the file being saved. 
     * @custom.post The file is saved.
     */
    public void saveText(final String aType, String aPath) {
        final String path = aPath + "." + aType;
        final JTextArea area = myPanel.getMyTextArea();
        
        try {
            final BufferedWriter fileOut = new BufferedWriter(new FileWriter(path));
            area.write(fileOut);
            
        } catch (final IOException e) {
            System.out.println("couldn't write a file");
        }
    }
    
    /**
     * Action listener for fonts. 
     * @author Christopher Helmer
     * @version 1
     *
     */
    private class TextAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent anEvent) {
            if (anEvent.getSource() == myStyle) {
                final String[] choices = {"Regular", "Bold", "Italics"};
                final String s = (String) JOptionPane.showInputDialog(null, 
                                  "choose your style", "style", 
                                  JOptionPane.INFORMATION_MESSAGE, 
                                  null, choices, choices[0]);
                if ("Italics".equals(s)) {
                    myArea.setFont(new Font(myArea.getFont().getName(),
                                            Font.ITALIC, myArea.getFont().getSize()));
                } else if ("Bold".equals(s)) {
                    myArea.setFont(new Font(myArea.getFont().getName(),
                                            Font.BOLD, myArea.getFont().getSize()));
                } else if ("Regular".equals(s)) {
                    myArea.setFont(new Font(myArea.getFont().getName(),
                                            Font.PLAIN, myArea.getFont().getSize()));
                }
                
            } else if (anEvent.getSource() == myType) {
                final String[] fonts = 
                        GraphicsEnvironment.getLocalGraphicsEnvironment().
                        getAvailableFontFamilyNames();
                final String s = (String) JOptionPane.showInputDialog(null, 
                                        "choose your font type", "type", 
                                        JOptionPane.INFORMATION_MESSAGE, 
                                        null, fonts, fonts[0]);
                
                myArea.setFont(new Font(s, myArea.getFont().getStyle(), 
                                        myArea.getFont().getSize()));
            } else if (anEvent.getSource() == myFontSize) {
                final String[] size = {"8", "10", "11", "12", "14", "16", 
                                       "18", "20", "24", "28", "32", "36", "42"};
                final String s = (String) JOptionPane.showInputDialog(null, 
                                         "choose your font size", "size", 
                                         JOptionPane.INFORMATION_MESSAGE, 
                                         null, size, size[0]);
                final int i = Integer.parseInt(s);
                myArea.setFont(new Font(myArea.getFont().getName(), 
                                        myArea.getFont().getStyle(), i));
            }
            
        }
        
    }
    
    /**
     * Action listener for saving and opening files.
     * @author Christopher Helmer
     * @version 1
     *
     */
    private class FileAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent anEvent) {
            if (myFilePicker == null) {
                myFilePicker = new JFileChooser();
            }
            
            final Object source = anEvent.getSource();
            
            int select = -1;
            myFilePicker.resetChoosableFileFilters();
            
            if (source == mySaveFile) {
                myFilePicker.setFileFilter(new FileNameExtensionFilter(
                                           "TXT format (*.txt)", "txt"));
                myFilePicker.setFileFilter(new FileNameExtensionFilter(
                                           "RTF format (*.rtf)", "rtf"));
                select = myFilePicker.showSaveDialog(null);
                
            } else if (source == myOpenFile) {
                myFilePicker.setFileFilter(new FileNameExtensionFilter(
                                           "Text Files", "txt"));
                                            
                select = myFilePicker.showOpenDialog(null);
            }
            
            final File result;
            
            if (select == JFileChooser.APPROVE_OPTION) {
                result = myFilePicker.getSelectedFile();
                if (result == null) {
                    return;
                }
                if (source == mySaveFile) {
                    final String extension = myFilePicker.getFileFilter().
                            getDescription().substring(0, 3);
                    
                    saveText(extension, result.getPath());
                    
                } else if (source == myOpenFile) {
                    openText(result);
                    
                }
            }   
        }  
    }  
}

