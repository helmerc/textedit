/**
 * 
 */
package eight.progpracticum;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Class that generates a panel with a text area inside a 
 * scroll pane for the text edit program.
 * @author Christopher Helmer
 * @version 1
 *
 */
@SuppressWarnings("serial")
public class TextEditPanel extends JPanel {
    /**
     * The number of rows in the text area.
     */
    private final int myRows = 20;
    /**
     * The number of columns in the text area.
     */
    private final int myColumns = 40;
    /**
     * Represents the text area.
     */
    private JTextArea myTextArea;
    /**
     * Represents the scroll pane.
     */
    private JScrollPane myScroll;
    
    /**
     * Constructor for the TextEditPanel.
     * @custom.post A new TextEditPanel is generated.
     */
    public TextEditPanel() {
        myTextArea = new JTextArea(myRows, myColumns);
        myScroll = new JScrollPane(myTextArea);
        textEditSetup();
        
    }
    /**
     * Helper method for the constructor.
     */
    private void textEditSetup() {
        myScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        myScroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        add(myScroll, BorderLayout.CENTER);
        
    }
    /**
     * Gets the text area.
     * @return the current text area.
     */
    public JTextArea getMyTextArea() {
        return myTextArea;
    }

}

