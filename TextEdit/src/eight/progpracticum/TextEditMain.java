/**
 * 
 */
package eight.progpracticum;

import java.awt.EventQueue;

/**
 * Starts text edit program.
 * 
 * @author Christopher Helmer
 * @version 1
 */
public final class TextEditMain {
  
  /**
   * Private constructor to inhibit instantiation.
   */
    private TextEditMain() {
        throw new IllegalStateException();
    }

  /**
   * Start point for the program.
   * 
   * @param aRgs command line arguments - ignored
   */
    public static void main(final String... aRgs) {

        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new TextEditGUI(); 
            }
        });
    }

}