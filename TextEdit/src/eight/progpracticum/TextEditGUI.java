/**
 * 
 */
package eight.progpracticum;

import java.awt.BorderLayout;

import javax.swing.JFrame;

/**
 * The Graphical User Interface for the text edit program.
 * 
 * @author Christopher Helmer
 * @version 1
 */
public class TextEditGUI {

    /** 
     * Represents a frame.
     */
    private final JFrame myGuiFrame;
    /**
     * Represents a top-level menu.
     */
    private final TextEditMenuBar myMenuBar;
    /**
     * Represents a panel.
     */
    private final TextEditPanel myPanel;
    
    /**
     * Construct the GUI.
     * @custom.post The text edit GUI is generated.
     */
    public TextEditGUI() {
        myGuiFrame = new JFrame("Chris' Notepad");
        myPanel = new TextEditPanel();
        myMenuBar = new TextEditMenuBar(myPanel);
        myGuiFrame.setJMenuBar(myMenuBar);
        myGuiFrame.add(myPanel, BorderLayout.CENTER);
        myGuiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        myGuiFrame.pack();
        myGuiFrame.setLocationRelativeTo(null);
        myGuiFrame.setVisible(true);
    }
}
